# NgxInfiniteSmoothCarousel

NgxInfiniteSmoothCarousel is a Angular library containing an infinite carousel to present images for your app.

This library was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.2.0.

![](https://gitlab.com/meadow_/ngx-infinite-smooth-carousel/-/raw/master/demo.gif)

## Installation

Use the package manager [npm](https://www.npmjs.com/) to install NgxInfiniteSmoothCarousel.

```bash
npm i ngx-infinite-smooth-carousel --save
```

## Usage

Main module file
```typescript
import { NgxInfiniteSmoothCarouselModule } from 'ngx-infinite-smooth-carousel';

@NgModule({
  imports: [
    NgxInfiniteSmoothCarouselModule,
  ],
})
export class AppModule { }
```

Component HTML
```html
<ngx-infinite-smooth-carousel [config]="config"></ngx-infinite-smooth-carousel>
```

Component TS
```typescript
  config : NgxIscConfig = {
    itemWidth: "150",          /* Width of each img in px */
    align: "middle",           /* OPTIONAL : Imgs vertical alignement: top | middle | bottom */
    spaceBetweenItems: "30",   /* OPTIONAL  : Space bewteen each items in px, it must be a multiple of ten */
    speed: "16",               /* OPTIONAL : Speed for a complete loop in seconds */
    stopOnHover: true,         /* OPTIONAL : Stop the animation if user hover the carousel */
    img:                       /* Array of img urls */
    [
      '/assets/imgs/afm_telethon.png',
      '/assets/imgs/afp_france_handicap.png',
      '/assets/imgs/ars_idf.png',
      '/assets/imgs/bpi.png',
      '/assets/imgs/essca.png',
      '/assets/imgs/handicap_international.png',
      '/assets/imgs/hizy.png',
      '/assets/imgs/lab_sante.png',
      '/assets/imgs/mobilite_reduite.png',
      '/assets/imgs/tangata.png',
      '/assets/imgs/ulysse_transport.png'
    ]
  }
```