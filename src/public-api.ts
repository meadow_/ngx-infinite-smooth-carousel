/*
 * Public API Surface of ngx-infinite-smooth-carousel
 */

export * from './lib/ngx-infinite-smooth-carousel.component';
export * from './lib/ngx-infinite-smooth-carousel.module';