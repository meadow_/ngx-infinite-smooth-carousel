import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NgxInfiniteSmoothCarouselComponent } from './ngx-infinite-smooth-carousel.component';

describe('NgxInfiniteSmoothCarouselComponent', () => {
  let component: NgxInfiniteSmoothCarouselComponent;
  let fixture: ComponentFixture<NgxInfiniteSmoothCarouselComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NgxInfiniteSmoothCarouselComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NgxInfiniteSmoothCarouselComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
