import { 
  AfterViewInit,
  Component, 
  ComponentFactoryResolver, 
  ComponentRef, 
  ElementRef, 
  HostBinding, 
  Injector, 
  Input, 
  OnInit, 
  Renderer2, 
  ViewChild, 
  ViewContainerRef 
} from '@angular/core';

// CMD 
// ng build ngx-infinite-smooth-carousel

export interface NgxIscConfig {
  /**
    * @name NgxIscConfig#itemWidth
    * @description Width of each img in px
  */
  itemWidth: string; 

  /**
    * @name NgxIscConfig#speed
    * @description speed for a total loop in seconds
  */
  speed?: string;

  /**
    * @name NgxIscConfig#stopOnHover
    * @description stop the animation if user hover the carousel
  */
  stopOnHover?: boolean;

  /**
    * @name NgxIscConfig#align
    * @description Imgs vertical alignement: top | middle | bottom
  */
  align?: string;

  /**
    * @name NgxIscConfig#spaceBetweenItems
    * @description Space bewteen each items in px, it must be a multiple of ten
  */
  spaceBetweenItems?: string;

  /**
    * @name NgxIscConfig#items
    * @description Array of carousel items 
  */
  items: Array<NgxIscImgItem>;
}
export interface NgxIscImgItem {

  /**
    * @name NgxIscImgItem#img
    * @description Path to img
  */
  url: string;

  /**
    * @name NgxIscImgItem#alt
    * @description Img alt
  */
  alt: string;
}

@Component({
  selector: 'ngx-infinite-smooth-carousel',
  template: `
    <div #wrapper class="nxg-isc-wrapper">
      <div #container class="nxg-isc-container">
        <ng-container #host></ng-container>
      </div>
    </div>
  `,
  styleUrls: ['./ngx-infinite-smooth-carousel.component.css']
})
export class NgxInfiniteSmoothCarouselComponent implements OnInit, AfterViewInit {

  @Input() config : NgxIscConfig;
  @ViewChild('host', { read: ViewContainerRef }) theHost: ViewContainerRef;
  private itemsRef: Array<ComponentRef<NxgIscItem>> = new Array<ComponentRef<NxgIscItem>>();

  @ViewChild("wrapper") wrapper: ElementRef;
  @ViewChild("container") container: ElementRef;

  @HostBinding('style.--smoothscroll-space') private animationSpace: string = '30px';
  @HostBinding('style.--smoothscroll-margin-left') private animationMarginLeft: string;
  @HostBinding('style.--smoothscroll-speed') private animationSpeed: string;

  constructor(private renderer: Renderer2, private resolver: ComponentFactoryResolver, private injector: Injector) { }

  ngOnInit(): void {
    this.config.items.forEach((item: NgxIscImgItem) => {
      const componentRef: ComponentRef<NxgIscItem> = this.createItem(item);
      this.itemsRef.push(componentRef);
    });
  }

  ngAfterViewInit() {
    this.itemsRef.forEach((itemRef: ComponentRef<NxgIscItem>) => {
      this.theHost.insert(itemRef.hostView);
    });

    if (this.config.stopOnHover) {
      this.renderer.addClass(this.wrapper.nativeElement, 'stop-on-hover'); 
    }

    // set space between items
    let itemSpace = 10;
    if (this.config.spaceBetweenItems) {
      itemSpace = parseFloat(this.config.spaceBetweenItems);
      setTimeout(() => {
        this.animationSpace = itemSpace.toString() + 'px';
      }, 500);
    }

    var sliderWidth = 0;
    const itemWidth: number = parseFloat(this.config.itemWidth) + (itemSpace*2);

    let animationWidth: number = 0;
    for (let index = 0; index < this.theHost.length; index++) {
      animationWidth += itemWidth;
    }

    // detect number of visible slides
    var containerWidth = this.wrapper.nativeElement.offsetWidth;
    var sliderOuterWidth = itemWidth;
    var slidesVisible = Math.ceil(containerWidth / sliderOuterWidth);

    // count slides to determine animation speed

    var speed: number = 0;
    if (this.config.speed) {
      speed = parseInt(this.config.speed);
    } else {
      var slidesNumber = this.theHost.length;
      speed = slidesNumber*2;
    }

    // append the tail
    const arraySlides = Array.prototype.slice.call(this.config.items);
    arraySlides.slice(0, slidesVisible).forEach((img, i) => {
      const componentRef: ComponentRef<NxgIscItem> = this.createItem(img);
      this.theHost.insert(componentRef.hostView);
    });

    // detect the slider width with appended tail
    for (let index = 0; index < this.theHost.length; index++) {
      sliderWidth += itemWidth;
    }

    this.renderer.setStyle(this.container.nativeElement, "height", this.config.itemWidth + "px");
    this.renderer.setStyle(this.container.nativeElement, "width", sliderWidth.toString() + "px");

    // update style values
    setTimeout(() => {
      this.animationMarginLeft = "-" + animationWidth.toString() + 'px';
      this.animationSpeed = speed.toString() + 's';
    }, 500);
  }

  private createItem(item: NgxIscImgItem) : ComponentRef<NxgIscItem> {
    const factory = this.resolver.resolveComponentFactory(NxgIscItem);
    const componentRef = factory.create(this.injector);
    componentRef.instance.url = item.url;
    componentRef.instance.alt = item.alt;
    componentRef.instance.width = this.config.itemWidth;
    componentRef.instance.align = this.config.align;
    componentRef.instance.spaceBetweenItems = this.config.spaceBetweenItems;
    return componentRef;
  }
}


@Component({
  selector: 'nxg-isc-item',
  template: ``,
})
export class NxgIscItem {
  url: string;
  alt: string;
  width: string;
  align: string;
  spaceBetweenItems: string;

  constructor(private renderer: Renderer2, private el: ElementRef) { }

  ngOnInit() {
    const imgtag = this.renderer.createElement("img");
    imgtag.src = this.url;
    imgtag.alt = this.alt;
    this.renderer.setStyle(imgtag, "width", this.width + "px");
    this.renderer.appendChild(this.el.nativeElement, imgtag);
    
    this.renderer.addClass(this.el.nativeElement, 'nxg-isc-item');
    this.renderer.setStyle(this.el.nativeElement, "display", "inline-block");
    if (this.align) {
      if (this.align == "top") {
        this.renderer.setStyle(this.el.nativeElement, "vertical-align", "top");
      }
      if (this.align == "middle") {
        this.renderer.setStyle(this.el.nativeElement, "vertical-align", "middle");
      }
      if (this.align == "bottom") {
        this.renderer.setStyle(this.el.nativeElement, "vertical-align", "bottom");
      }
    }
    this.renderer.setStyle(this.el.nativeElement, "width", "auto");
    this.renderer.setStyle(this.el.nativeElement, "float", "none");
    if (this.spaceBetweenItems) {
      this.renderer.setStyle(this.el.nativeElement, "padding", "0 " + this.spaceBetweenItems +"px");
    } else {
      this.renderer.setStyle(this.el.nativeElement, "padding", "0 10px");
    }
  }
}