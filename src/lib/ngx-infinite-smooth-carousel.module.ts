import { NgModule } from '@angular/core';
import { NgxInfiniteSmoothCarouselComponent } from './ngx-infinite-smooth-carousel.component';


@NgModule({
  declarations: [NgxInfiniteSmoothCarouselComponent],
  imports: [
  ],
  exports: [NgxInfiniteSmoothCarouselComponent]
})
export class NgxInfiniteSmoothCarouselModule { }
